/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package lab.dev.maven.plugins;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.maven.model.Resource;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.resources.ResourcesMojo;
import org.apache.maven.shared.filtering.MavenFilteringException;
import org.apache.maven.shared.filtering.MavenResourcesExecution;
import org.apache.maven.shared.utils.io.FileUtils;

import lab.dev.maven.plugins.utils.Enumeres;
import lab.dev.maven.plugins.utils.MavenUtils;

/**
 * In a nutshell, this implementation perform :
 * <ul>
 * <li>template concatenation,</li>
 * <li>Yaml like / Ansible properties injection</li>
 * <li>and filtering of Jinja 2 '{{ variables }}', @see also {@link AnsibleFilterWrapper}.</li>
 * </ul>
 * 
 * Please refer to the goal and usage documentation for detailed behavior and configuration.
 * 
 * @version $Revision$ $Date$
 */
@Mojo(name = Enumeres.PLUGIN.MVN_GOAL.EXTENDED_FILTERING, defaultPhase = LifecyclePhase.PROCESS_RESOURCES)
public class ExtendedFilteringMojo extends ResourcesMojo {


	/**
	 * Main parameter of the extended-filtering, allows to configure templates, inputFiles, encoding and outputFiles.
	 */
	@Parameter(required = true)
	private List<ExtendedFilteringResource> extendedFilteringResources;

	/**
	 * The build output directory.
	 */
	@Parameter(defaultValue = "${project.build.outputDirectory}", readonly = true, required = true)
	protected File buildOutputDirectory;

	/**
	 * The directory used by the plugin.
	 */
	@Parameter(defaultValue = "${project.build.directory}" + Enumeres.PLUGIN.PLUGIN_DIR, readonly = true, required = true)
	protected File pluginDirectory;


	/**
	 * Main trigger for "extended-filtering" inside the Maven system. <br>
	 * The job here is to iterate over the resources configured in the plugin parameters and call accordingly the following
	 * subroutines
	 * <ul>
	 * <li>initializeResourceFromMojoEnvironment</li>
	 * <li>concatenateTemplates</li>
	 * <li>executeExtendedFiltering</li>
	 * </ul>
	 * 
	 * @throws MojoExecutionException
	 *             the mojo execution exception
	 */
	public void execute() throws MojoExecutionException {

		// Here we go..

		for (ExtendedFilteringResource extFilteringResource : extendedFilteringResources) {

			initializeResourceFromMojoEnvironment(extFilteringResource);

			logResourceExecutionHeader(extFilteringResource);

			concatenateTemplates(extFilteringResource);

			executeExtendedFiltering(extFilteringResource);

			MavenUtils.deleteFolder(pluginDirectory);
		}
	}


	/**
	 * Initialize the given resource from this maven plugin environment.
	 *
	 * @param resource
	 *            the extended filtering resource
	 * @throws MojoExecutionException
	 *             the mojo execution exception
	 */
	private void initializeResourceFromMojoEnvironment(ExtendedFilteringResource resource) throws MojoExecutionException {

		// Resolve and prepare absolute or relative output file

		resource.setOutputFile(MavenUtils.initializeOutputPath(resource.getOutputFile(), buildOutputDirectory));
		resource.setOutputFolder(MavenUtils.initializeOutputPath(resource.getOutputFolder(), buildOutputDirectory));

		// Clean and create a fresh temporary folder

		if (MavenUtils.deleteFolder(pluginDirectory) && !pluginDirectory.mkdirs()) {

			throw new MojoExecutionException(Enumeres.EXCEPTION.CANNOT_CREATE_TEMPORARY_FOLDER_EXCEPTION + pluginDirectory);
		}
	}


	/**
	 * Create the usual maven log header with inputs and outputs for resource transformation.
	 *
	 * @param resource
	 *            the extended filtering resource
	 */
	private void logResourceExecutionHeader(ExtendedFilteringResource resource) {

		getLog().info(Enumeres.LOG.INPUT_DATA_FILES + resource.getInputDataFileSets().toString());

		getLog().info(Enumeres.LOG.INPUT_TEMPLATES + resource.getTemplatesFileSets().toString());

		getLog().info(Enumeres.LOG.GENERATING_OUTPUT);
	}


	/**
	 * Concatenate all the declared templates of a given extFilteringResource. Concatenation is activated only if an OutputFile
	 * parameter is provided.
	 * 
	 * @param extResource
	 *            the extended filtering resource
	 * @throws MojoExecutionException
	 *             the mojo execution exception
	 */
	private void concatenateTemplates(ExtendedFilteringResource extResource) throws MojoExecutionException {

		if (extResource.getOutputFile() != null) {

			File concatenatedResultFile = new File(pluginDirectory, new File(extResource.getOutputFile()).getName());

			for (String templateFileSet : extResource.getTemplatesFileSets()) {

				concatenateTemplate(extResource, concatenatedResultFile, templateFileSet);
			}
		}
	}


	/**
	 * Concatenate template :
	 * <ul>
	 * <li>resolve the given templateFileSet</li>
	 * <li>append the different resolved file with a line separator between each file content</li>
	 * </ul>
	 * 
	 * @param extResource
	 *            the ext resource
	 * @param concatenatedResultFile
	 *            the concatenated result file
	 * @param templateFileSet
	 *            the template file set
	 * @throws MojoExecutionException
	 *             the mojo execution exception
	 */
	private void concatenateTemplate(ExtendedFilteringResource extResource, File concatenatedResultFile, String templateFileSet)
			throws MojoExecutionException {

		for (File template : MavenUtils.resolveFileSet(templateFileSet, buildOutputDirectory)) {

			try { // Simple file appender usage with insertion of a line separator between each file content

				String resultFile = concatenatedResultFile.getAbsolutePath(), encoding = extResource.getEncoding();

				FileUtils.fileAppend(resultFile, encoding, FileUtils.fileRead(template) + System.getProperty("line.separator"));
			}
			catch (IOException e) {
				throw new MojoExecutionException(Enumeres.EXCEPTION.FILE_WRITE_EXCEPTION + template.getAbsolutePath(), e);
			}
		}
	}


	/**
	 * Execute extended filtering on two different target :
	 * <ul>
	 * <li>First, results are concatenated to a single file, if an outputFile parameter is supplied</li>
	 * <li>Then the results are stored without concatenation, if an outputFolder parameter is supplied</li>
	 * </ul>
	 * .
	 *
	 * @param extResource
	 *            the ext resource
	 * @throws MojoExecutionException
	 *             the mojo execution exception
	 */
	private void executeExtendedFiltering(ExtendedFilteringResource extResource) throws MojoExecutionException {

		try {

			List<String> inputDataFiles = MavenUtils.resolveFileSets(extResource.getInputDataFileSets(), buildOutputDirectory);

			String encoding = extResource.getEncoding();

			// If an outputFile parameter is supplied, results are concatenated to a single file

			if (extResource.getOutputFile() != null) {

				File concatenatedOutputDir = new File(extResource.getOutputFile()).getParentFile();

				executeExtendedFiltering(inputDataFiles, concatenatedOutputDir, getTemplateResource(), encoding);
			}

			// If an outputFolder parameter is supplied, results are stored without concatenation

			if (extResource.getOutputFolder() != null) {

				File outputDirectory = new File(extResource.getOutputFolder());

				executeExtendedFiltering(inputDataFiles, outputDirectory, getTemplatesResources(extResource), encoding);
			}
		}
		catch (MavenFilteringException e) {
			throw new MojoExecutionException(e.getMessage(), e);
		}
	}


	/**
	 * Prepare call and execute org.apache.maven.shared.maven-filtering with a custom AnsibleFilter.<br />
	 *
	 * @param inputFiles
	 *            the input files
	 * @param outputDirectory
	 *            the output directory
	 * @param templates
	 *            the concatenated template
	 * @param encoding
	 *            the encoding
	 * @throws MavenFilteringException
	 *             the maven filtering exception
	 * @see {@link AnsibleFilterWrapper}
	 * @see <a href="https://maven.apache.org/shared/maven-filtering/usage.html">Maven-filtering usage</a>
	 */
	private void executeExtendedFiltering(List<String> inputFiles, File outputDirectory, List<Resource> templates,
			String encoding) throws MavenFilteringException {

		// Initialize a MavenResourcesExecution with the plugin configuration parameters

		MavenResourcesExecution mavenResourcesExecution = new MavenResourcesExecution(templates, outputDirectory, project,
				encoding, inputFiles, Collections.<String>emptyList(), session);

		// Add a custom AnsibleFilterWrapper and the default mavenResourcesExecution configuration

		mavenResourcesExecution.addFilterWrapper(new AnsibleFilterWrapper(inputFiles, encoding));
		mavenResourcesExecution = configureDefaultResourceExec(mavenResourcesExecution);

		// Finally execute the resource filtering

		mavenResourcesFiltering.filterResources(mavenResourcesExecution);
	}


	/**
	 * Get the template resource definition passed to the filtering process : <br />
	 * Define the concatenated template temporary folder as the resource to be filtered.
	 *
	 * @return the template directory resource
	 */
	private List<Resource> getTemplateResource() {

		List<Resource> templates = new ArrayList<Resource>();
		Resource resource = new Resource();
		resource.setDirectory(pluginDirectory.getAbsolutePath());
		resource.setFiltering(true);
		templates.add(resource);

		return templates;
	}


	/**
	 * Get the templates resources definitions passed to the filtering process : <br />
	 * Define the template fileSets as the resources to be resolved and filtered.
	 *
	 * @param extResource
	 *            the ext resource
	 * @return the template directory resource
	 * @throws MojoExecutionException
	 *             the mojo execution exception
	 */
	private List<Resource> getTemplatesResources(ExtendedFilteringResource extResource) throws MojoExecutionException {

		List<Resource> result = new ArrayList<Resource>();

		for (String fileSet : extResource.getTemplatesFileSets()) {

			Resource resource = new Resource();
			String[] baseDirAndIncludes = MavenUtils.getBaseDirAndIncludes(fileSet);
			resource.setIncludes(Arrays.asList(new String[] { baseDirAndIncludes[1] }));
			resource.setDirectory(baseDirAndIncludes[0]);
			resource.setFiltering(true);
			result.add(resource);
		}

		return result;
	}


	/**
	 * Default mavenResourcesExecution initialization <br />
	 * <a href="https://maven.apache.org/shared/maven-filtering/usage.html">maven-filtering usage</a>
	 *
	 * @param mavenResourcesExecution
	 *            the maven resources execution
	 * @return the maven resources execution
	 */
	private MavenResourcesExecution configureDefaultResourceExec(MavenResourcesExecution mavenResourcesExecution) {

		mavenResourcesExecution.setEscapeWindowsPaths(true);
		mavenResourcesExecution.setInjectProjectBuildFilters(false);
		mavenResourcesExecution.setOverwrite(true);
		mavenResourcesExecution.setAddDefaultExcludes(addDefaultExcludes);
		mavenResourcesExecution.setDelimiters(delimiters, useDefaultDelimiters);

		return mavenResourcesExecution;
	}
}
