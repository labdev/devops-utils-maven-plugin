/**
 * Provides reusable static utility methods and constants related to this Maven Plugin execution context.
 */
package lab.dev.maven.plugins.utils;