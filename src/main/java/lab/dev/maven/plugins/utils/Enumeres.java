/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package lab.dev.maven.plugins.utils;

/*
 * Copyright 2013 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0,
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Enumeration of String constants classified under interface declarations. <br>
 * It's helping to keep the source code clean, regardless of the corresponding and inconsistent PMD violation.
 * 
 * @version $Revision$ $Date$
 */
public interface Enumeres {


	/**
	 * Enumeration of the exception used in the project.
	 */
	interface EXCEPTION {


		/** The file write exception. */
		String FILE_WRITE_EXCEPTION = "An exception occured, unable to write file : ";

		/** The file read exception. */
		String FILE_READ_EXCEPTION = "An exception occured, unable to read file : ";

		/** The file delete exception. */
		String FILE_DELETE_EXCEPTION = "An exception occured, unable to delete file : ";

		/** The file not found exception. */
		String FILE_NOT_FOUND_EXCEPTION = "An exception occured, file not found : ";

		/** The cannot create output directory exception. */
		String CANNOT_CREATE_OUTPUT_DIRECTORY_EXCEPTION = "Cannot create resource output directory : ";

		/** The cannot create temporary folder exception. */
		String CANNOT_CREATE_TEMPORARY_FOLDER_EXCEPTION = "Cannot create temporary folder: ";

	}

	/**
	 * Enumeration of the log Strings used in the project.
	 */
	interface LOG {


		/** The duplicated property warn. */
		String DUPLICATED_PROPERTY_WARN = "Duplicated property \"{0}\" - value : \"{1}\" will be replace by : \"{2}\"";

		/** The loading property log message. */
		String LOADING_PROPERTY = "Loading input property [ {0} : {1} ]";

		/** The generating output folder. */
		String GENERATING_OUTPUT = " Generating output..";

		/** The templates. */
		String INPUT_TEMPLATES = " Templates   : ";

		/** The input files. */
		String INPUT_DATA_FILES = " Input files : ";

	}

	/**
	 * Enumeration of constants used in the plugin.
	 */
	interface PLUGIN {


		/**
		 * Enumeration of the goals used in the plugin.
		 */
		interface MVN_GOAL {


			/** The extended filtering goal. */
			String EXTENDED_FILTERING = "extended-filtering";

			// ..
		}

		/** The plugin dir in the maven target. */
		String PLUGIN_DIR = "/devops-utils/";
	}
}
