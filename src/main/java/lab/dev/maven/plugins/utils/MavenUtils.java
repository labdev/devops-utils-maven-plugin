/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package lab.dev.maven.plugins.utils;

/*
 * Copyright 2013 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0,
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.shared.utils.io.FileUtils;
import org.codehaus.plexus.util.DirectoryScanner;

/**
 * This utility class provides basic operations on File within Maven Plugins execution contexts.
 * 
 * @version $Revision$ $Date$
 */
public final class MavenUtils {


	/**
	 * Private constructor prevent the class from being explicitly instantiated.
	 */
	private MavenUtils() {}




	/**
	 * Initialize a given output path :
	 * <ul>
	 * <li>resolve the path if relative</li>
	 * <li>append the different resolved file with a line separator between each file content</li>
	 * </ul>
	 *
	 * @param outputPath the output path
	 * @return the string
	 * @throws MojoExecutionException the mojo execution exception
	 */
	public static String initializeOutputPath(String outputPath, File relativeTo) throws MojoExecutionException {

		if (outputPath != null) {

			File outputFile = MavenUtils.resolveRelativePath(outputPath, relativeTo);

			outputPath = outputFile.getAbsolutePath();

			if (!outputFile.getParentFile().exists() && !outputFile.getParentFile().mkdirs()) {

				throw new MojoExecutionException(Enumeres.EXCEPTION.CANNOT_CREATE_OUTPUT_DIRECTORY_EXCEPTION + outputFile);
			}
		}

		return outputPath;
	}

	
	/**
	 * Iterate and call resolveFileSet subroutines over a given list of fileSet. Manage non absolute path references relatively to
	 * the given {@code relativeTo} file parameter. and also manage wild card resolution : @see {@link #resolveFileSet}
	 *
	 * @param fileSets
	 *            the list of fileSet
	 * @param relativeTo
	 *            the reference path used to managed the relative file
	 * @return the list
	 * @throws MojoExecutionException
	 *             if the target file not exist or cannot be acceded
	 * @see MavenUtils#resolveFileSet(String, File)
	 */
	public static List<String> resolveFileSets(List<String> fileSets, File relativeTo) throws MojoExecutionException {

		List<String> result = new ArrayList<String>();

		for (String fileSet : fileSets) {

			for (File file : resolveFileSet(fileSet, relativeTo)) {
				result.add(file.getAbsolutePath());
			}
		}

		return result;
	}


	/**
	 * Resolve a file : Manage non absolute path references relatively to the given {@code relativeTo} file parameter. Also manage
	 * sub-directory and file resolution if the given fileSet contains criteria. The following special characters can be used as
	 * wild cards:
	 * <ul>
	 * <li>'*' matches zero or more characters of the filename
	 * <li>'**' is used for a path segment in the pattern, it matches zero or more path segments of the name.
	 * </ul>
	 * As the result files list is expected with existing files, this implementation can throw FILE_NOT_FOUND_EXCEPTION and
	 * FILE_READ_EXCEPTION if the target files not exist or cannot be acceded
	 *
	 * @param fileSetStr
	 *            the file set str
	 * @param relativeTo
	 *            the reference path used to managed the relative file
	 * @return the file
	 * @throws MojoExecutionException
	 *             if the target file not exist or cannot be acceded
	 */
	public static List<File> resolveFileSet(String fileSetStr, File relativeTo) throws MojoExecutionException {

		// Manage non absolute references relatively to the build output directory

		File fileSet = resolveRelativePath(fileSetStr, relativeTo);

		// Resolve wildcards in the path if any

		return fileSetScan(fileSet.getAbsolutePath());
	}


	/**
	 * Scan a directory for files/directories which match the given fileSet criteria.<br>
	 * The following special characters can be used in the fileSet:
	 * <ul>
	 * <li>'*' matches zero or more characters of the filename
	 * <li>'**' is used for a path segment in the pattern, it matches zero or more path segments of the name.
	 * </ul>
	 * 
	 * @param fileSet
	 *            the fileSet
	 * @return the list of resolved files
	 * @throws MojoExecutionException
	 *             if the target file not exist or cannot be acceded
	 */
	private static List<File> fileSetScan(String fileSet) throws MojoExecutionException {

		List<File> result = new ArrayList<File>();

		String[] baseDirAndIncludes = getBaseDirAndIncludes(fileSet);

		DirectoryScanner directorscannery = new DirectoryScanner();
		directorscannery.setBasedir(new File(baseDirAndIncludes[0]));
		directorscannery.setIncludes(new String[] { baseDirAndIncludes[1] });
		directorscannery.scan();

		for (String includedFile : directorscannery.getIncludedFiles()) {

			result.add(resolveAndCheckFile(includedFile, directorscannery.getBasedir()));
		}

		return result;
	}


	/**
	 * Extract the base directory and includes criteria from a given fileSet. If the fileSet contains a '*' wild card, sub-divide
	 * the already resolved path (the base directory), and the regular expression containing the wild cards characters (the
	 * includes part), Otherwise, 'includes part' is just the file name.
	 * 
	 * @param fileSet
	 *            the file set
	 * @return the base directory and the includes of the fileSet
	 */
	public static String[] getBaseDirAndIncludes(String fileSet) {

		String[] result = null;

		// If the fileSet use wild cards, the 'includes part' start a the first '*' character.

		int index = fileSet.indexOf("*");
		if (index > 0) {

			result = new String[] { fileSet.substring(0, index), fileSet.substring(index, fileSet.length()) };
		}

		else { // Otherwise, just divide the parent directory path and the file name

			File file = new File(fileSet);
			result = new String[] { file.getParent(), file.getName() };
		}

		return result;
	}


	/**
	 * Manage non absolute path references relatively to the given {@code relativeTo} file parameter.
	 *
	 * @param fileStr
	 *            the String representing the file path
	 * @param relativeTo
	 *            the reference path used to managed the relative file
	 * @return the file
	 */
	public static File resolveRelativePath(String fileStr, File relativeTo) {

		File file = new File(fileStr);

		if (!file.isAbsolute()) {
			file = new File(relativeTo, file.getPath());
		}

		return file;
	}


	/**
	 * Get a File from a String representation and throw exception if the corresponding file not exist or cannot be acceded.
	 *
	 * @param templateStr
	 *            the template str
	 * @param baseDirectory 
	 * @return the File representation of the given
	 * @throws MojoExecutionException
	 *             the mojo execution exception
	 */
	private static File resolveAndCheckFile(String templateStr, File relativeTo) throws MojoExecutionException {

		File file = new File(relativeTo, templateStr);

		if (!file.exists()) {
			throw new MojoExecutionException(Enumeres.EXCEPTION.FILE_NOT_FOUND_EXCEPTION + file.getAbsolutePath());
		}
		if (!file.isFile() || !file.canRead()) {
			throw new MojoExecutionException(Enumeres.EXCEPTION.FILE_READ_EXCEPTION + file.getAbsolutePath());
		}

		return file;
	}


	/**
	 * Delete recursively the given folder.
	 *
	 * @param folder
	 *            the folder to delete
	 * @return true, if successful
	 * @throws MojoExecutionException
	 *             if the folder cannot be deleted
	 */
	public static boolean deleteFolder(File folder) throws MojoExecutionException {

		try {
			if (folder.exists()) {
				FileUtils.deleteDirectory(folder);
			}
		}
		catch (IOException e) {
			throw new MojoExecutionException(Enumeres.EXCEPTION.FILE_DELETE_EXCEPTION + folder, e);
		}

		return true;
	}
}
