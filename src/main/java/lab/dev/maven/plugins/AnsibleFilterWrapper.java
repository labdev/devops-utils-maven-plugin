/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package lab.dev.maven.plugins;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.maven.plugin.logging.SystemStreamLog;
import org.apache.maven.shared.utils.io.FileUtils.FilterWrapper;
import org.codehaus.plexus.interpolation.Interpolator;
import org.codehaus.plexus.interpolation.InterpolatorFilterReader;
import org.codehaus.plexus.interpolation.MapBasedValueSource;
import org.codehaus.plexus.interpolation.RegexBasedInterpolator;

import lab.dev.maven.plugins.utils.Enumeres;

/**
 * An 'Ansible / YAML like' filter implementation to use with the 'shared maven-filtering' component.
 * <ul>
 * <li>interpolation is based on Jinja2 '{{ variable }}' substitution</li>
 * <li>property value injection is delegated to the inner readAnsiblePropertiesFiles() subroutines</li>
 * </ul>
 * 
 * @version $Revision$ $Date$
 */
public class AnsibleFilterWrapper extends FilterWrapper {


	/** The Logger. */
	SystemStreamLog logger = new SystemStreamLog();

	/** The Ansible properties files. */
	private List<String> ansiblePropertiesFiles;

	/** The encoding. */
	private String encoding;

	// Constants :

	/** The Constant PROPERTY_HOLDER_START_TOKEN. */
	private static final String PROPERTY_HOLDER_START_TOKEN = "{{";

	/** The Constant PROPERTY_HOLDER_END_TOKEN. */
	private static final String PROPERTY_HOLDER_END_TOKEN = "}}";

	/** The Constant PROPERTY_HOLDER_START_REGEXP. */
	private static final String PROPERTY_HOLDER_START_REGEXP = "\\{\\{\\s";

	/** The Constant PROPERTY_HOLDER_END_REGEXP. */
	private static final String PROPERTY_HOLDER_END_REGEXP = "(.+?)\\s\\}\\}";

	/** The Constant YAML_COMMENT_REGEXP. */
	private static final String YAML_COMMENT_REGEXP = "^(\\s)*#.*";

	/** The Constant YAML_SEPARATOR. */
	private static final char YAML_SEPARATOR = ':';


	/**
	 * Instantiates a new ansible filter wrapper.
	 *
	 * @param ansiblePropertiesFiles
	 *            the ansible properties files
	 * @param encoding
	 *            the encoding
	 */
	public AnsibleFilterWrapper(List<String> ansiblePropertiesFiles, String encoding) {
		super();
		this.ansiblePropertiesFiles = ansiblePropertiesFiles;
		this.encoding = encoding;
	}


	/**
	 * Gets the InterpolatorFilterReader for ansible properties files.
	 * <ul>
	 * <li>interpolation is based on Jinja2 '{{ variable }}' substitution</li>
	 * <li>property value injection is delegated to the readAnsiblePropertiesFiles() subroutines</li>
	 * </ul>
	 * 
	 * @param reader
	 *            the reader
	 * @return the reader
	 * @see org.apache.maven.shared.utils.io.FileUtils.FilterWrapper#getReader(java.io.Reader)
	 */
	public Reader getReader(Reader reader) {

		Interpolator interpolator = new RegexBasedInterpolator(PROPERTY_HOLDER_START_REGEXP, PROPERTY_HOLDER_END_REGEXP);

		interpolator.addValueSource(readAnsiblePropertiesFiles());

		return new InterpolatorFilterReader(reader, interpolator, PROPERTY_HOLDER_START_TOKEN, PROPERTY_HOLDER_END_TOKEN);
	}


	/**
	 * Read the list of Ansible properties files of this wrapper instance.
	 *
	 * @return the map based value source
	 */
	protected MapBasedValueSource readAnsiblePropertiesFiles() {

		Map<String, String> map = new HashMap<String, String>();

		for (String file : ansiblePropertiesFiles) {

			try {
				readAnsiblePropertiesFile(map, file);
			}
			catch (FileNotFoundException e) {
				throw new RuntimeException(Enumeres.EXCEPTION.FILE_NOT_FOUND_EXCEPTION + file, e);
			}
			catch (IOException e) {
				throw new RuntimeException(Enumeres.EXCEPTION.FILE_READ_EXCEPTION + file, e);
			}
		}

		return new MapBasedValueSource(map);
	}


	/**
	 * Read an Ansible properties file. Iterate over each line and call readAnsibleProperty subroutine.
	 *
	 * @param map
	 *            the map
	 * @param file
	 *            the file
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	protected void readAnsiblePropertiesFile(Map<String, String> map, String file) throws FileNotFoundException, IOException {

		FileInputStream fileInputStream = new FileInputStream(file);
		InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, encoding);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

		for (String line; (line = bufferedReader.readLine()) != null;) {

			readAnsibleProperty(map, line);
		}

		bufferedReader.close();
		inputStreamReader.close();
		fileInputStream.close();
	}


	/**
	 * Read an Ansible property line. The YAML semantic is not fully implemented here, I've arbitrarily choose to use a really
	 * simple implementation (instead of SnakeYaml, YamlBean, Jackson, etc..), but for now, it's more than enough for parsing
	 * simple linear properties declarations.
	 *
	 * @param map
	 *            the map
	 * @param line
	 *            the line
	 */
	protected void readAnsibleProperty(Map<String, String> map, String line) {
		int separatorIndex;

		// Simple by-pass of the commented lines, getting the key/value around the separator, and apply a specific trim,
		// before registering the ansible properties.

		if (!line.matches(YAML_COMMENT_REGEXP) && (separatorIndex = line.indexOf(YAML_SEPARATOR)) > 0) {

			String key = ansiblePropertiesTrim(line.substring(0, separatorIndex));
			String value = ansiblePropertiesTrim(line.substring(separatorIndex + 1, line.length()));

			logger.debug(MessageFormat.format(Enumeres.LOG.LOADING_PROPERTY, key, value));

			// Log a warn when a specific key is duplicated

			if (map.get(key) != null) {
				logger.warn(MessageFormat.format(Enumeres.LOG.DUPLICATED_PROPERTY_WARN, key, map.get(key), value));
			}

			map.put(key, value);
		}
	}


	/**
	 * Ansible properties trim : suppress whitespace and quote characters at the beginning and the end of a given value.
	 *
	 * @param value
	 *            the value
	 * @return the string
	 */
	private String ansiblePropertiesTrim(String value) {

		return value.replaceAll("(^(\\s|\")*)|((\\s|\")*$)", "");
	}
}
