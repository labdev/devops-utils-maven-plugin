/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package lab.dev.maven.plugins;

import java.util.List;

/**
 * The Class ExtendedFilteringResource just describes the inputs, templates, encoding and output parameters of an 'extended
 * filtering'
 * 
 * @version $Revision$ $Date$
 */
public class ExtendedFilteringResource {


	/**
	 * The templates with Jinja2 '{{ variable }}' placeholders.
	 */
	private List<String> templatesFileSets;

	/**
	 * The properties input files used for values injection.
	 */
	private List<String> inputDataFileSets;

	/**
	 * The filtered output file.
	 */
	private String outputFile;

	/**
	 * The filtered output file.
	 */
	private String outputFolder;

	/**
	 * The encoding used to parse and write given files.
	 */
	private String encoding;


	/**
	 * @return the templatesFileSets
	 */
	public List<String> getTemplatesFileSets() {
		return templatesFileSets;
	}


	/**
	 * @param templatesFileSets
	 *            the templatesFileSets to set
	 */
	public void setTemplatesFileSets(List<String> templatesFileSets) {
		this.templatesFileSets = templatesFileSets;
	}


	/**
	 * @return the inputDataFileSets
	 */
	public List<String> getInputDataFileSets() {
		return inputDataFileSets;
	}


	/**
	 * @param inputDataFileSets
	 *            the inputDataFileSets to set
	 */
	public void setInputDataFileSets(List<String> inputDataFileSets) {
		this.inputDataFileSets = inputDataFileSets;
	}


	/**
	 * @return the outputFile
	 */
	public String getOutputFile() {
		return outputFile;
	}


	/**
	 * @param outputFile
	 *            the outputFile to set
	 */
	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}


	/**
	 * @return the outputFolder
	 */
	public String getOutputFolder() {
		return outputFolder;
	}


	/**
	 * @param outputFolder
	 *            the outputFolder to set
	 */
	public void setOutputFolder(String outputFolder) {
		this.outputFolder = outputFolder;
	}


	/**
	 * @return the encoding
	 */
	public String getEncoding() {
		return encoding;
	}


	/**
	 * @param encoding
	 *            the encoding to set
	 */
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

}
