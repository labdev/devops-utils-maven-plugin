/**
 * Main package of this plugin; Provides simple mojo(..s) to help and simplify integration of DevOps process.
 * <ul>
 * <li>extended-filtering : {@link lab.dev.maven.plugins.ExtendedFilteringMojo}</li>
 * <li>next goals are coming soon...</li>
 * </ul>
 */
package lab.dev.maven.plugins;