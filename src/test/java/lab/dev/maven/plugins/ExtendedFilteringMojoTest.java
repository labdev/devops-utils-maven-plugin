/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package lab.dev.maven.plugins;

import java.io.File;
import java.io.IOException;

import org.apache.maven.execution.DefaultMavenExecutionRequest;
import org.apache.maven.execution.MavenExecutionRequest;
import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuilder;
import org.apache.maven.project.ProjectBuildingRequest;
import org.apache.maven.shared.utils.io.FileUtils;
import org.junit.Test;

/**
 * The Class ExtendedFilteringMojoTest.
 */
public class ExtendedFilteringMojoTest extends AbstractMojoTestCase {


	/**
	 * The ExtendedFilteringMojo
	 */
	private ExtendedFilteringMojo extFilteringMojo;
	

	/** The Constant outputPath. */
	private final static String OUTPUT_DIR = "/target/test-classes/";


	/**
	 * Test mojo lookup, test harness should be working fine.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testHarnessEnvironmentAndPluginUsage() throws Exception {

		initializeExtFilteringMojo("/pom-extFiltering-usage.xml");

		assertNotNull(extFilteringMojo);

	}


	/**
	 * Test the ExtFilteringMojo execution with an single output.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testSingleOutput() throws Exception {

		initializeExtFilteringMojo("/pom-extFiltering-singleOutput.xml");

		extFilteringMojo.execute();

		assertAndCompareOutput("test-result-singleOutput");
	}


	/**
	 * Test the ExtFilteringMojo execution with multiples outputs.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testMultipleOutputs() throws Exception {

		initializeExtFilteringMojo("/pom-extFiltering-multipleOutputs.xml");

		extFilteringMojo.execute();

		assertAndCompareOutput("test-result-multipleOutputs1");

		assertAndCompareOutput("test-result-multipleOutputs2");

	}


	/**
	 * Test the ExtFilteringMojo execution with a folder output and input fileSet with wild cards.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testFolderOutputAndWildCard() throws Exception {

		initializeExtFilteringMojo("/pom-extFiltering-folderOutputAndWildCard.xml");

		extFilteringMojo.execute();

		assertAndCompareOutput("test-result-wildCardAndConcatenatedOutput");

		assertAndCompareOutput("test-result-outputFolder/template-example2");

		assertAndCompareOutput("test-result-outputFolder/template-example3");

		assertAndCompareOutput("test-result-outputFolder/wildcard-test-subdirectory/template-example4");
	}


	/**
	 * Initialize a Maven project context for testing purpose. Plugin configuration and test case parameters come from the given
	 * nputPom parameter
	 *
	 * @param inputPom
	 *            the input pom
	 * @throws Exception
	 *             the exception
	 */
	protected void initializeExtFilteringMojo(String inputPom) throws Exception {

		File testPom = new File(getBasedir(), OUTPUT_DIR + inputPom);

		MavenExecutionRequest executionRequest = new DefaultMavenExecutionRequest();
		ProjectBuildingRequest buildingRequest = executionRequest.getProjectBuildingRequest();

		ProjectBuilder projectBuilder = this.lookup(ProjectBuilder.class);
		MavenProject project = projectBuilder.build(testPom, buildingRequest).getProject();

		// Override build parameters to get tests working with /target/test-classes outputDirectory

		project.getBuild().setOutputDirectory(getBasedir() + OUTPUT_DIR);

		extFilteringMojo = (ExtendedFilteringMojo) this.lookupConfiguredMojo(project, "extended-filtering");
	}


	/**
	 * Assert and compare output.
	 *
	 * @param expectedResultFileStr
	 *            the expected result
	 * @param fileName
	 *            the results
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void assertAndCompareOutput(String fileName) throws IOException {

		File resultFile = new File(getBasedir(), OUTPUT_DIR + fileName);
		File expectedResultFile = new File(getBasedir(), OUTPUT_DIR + "expected-result/expected-" + fileName);

		// Just a file exist and compare to expected result assert here
		// Cause most of the cleverness of the test cases are in the given pom, inputfiles and templates

		assertTrue(resultFile.exists());
		assertEquals(FileUtils.fileRead(expectedResultFile), FileUtils.fileRead(resultFile));
	}

}
