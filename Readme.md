devops-utils maven plugin
=========================

## About

Nothing extraordinary here, The "DevOps - Utils Maven Plugins" just provides simple utilities to help and simplify integration of DevOps process. For now, the available 'maven goals' are :

- extended-filtering : this mojo is an extension of the maven-resources-plugin (using the shared maven-filtering component) that allows to work on Ansible templates. A good use case is to use it for keeping the same templating models on your developement, test or production environnements, to avoid tedious change of format which is often a time wasting and error prone. Technically, the plugin just reads values declarations from YAML-like properties and process substitution based on the Jinja2 {{ variables }} interpolation. 

- TODO.. (Rome wasn't built in a day ;)

## Build

Currently, you need Maven 3.x to build the plugin.

* Install and tests :

```
mvn clean install
```

* Javadoc generation :

```
mvn javadoc:javadoc
```

## Runtime

Brief example of how to use the extended-filtering goals. Refer to the documentation options section to feet this given configuration to your need :

Configure the plugin something like this if you intend to bind it to your build:

```xml
<project>
  [...]
	<build>
		<plugins>
			<plugin>
				<groupId>lab.dev.maven.plugins</groupId>
				<artifactId>devops-utils</artifactId>
				<version>1.0.0</version>
				<configuration>
					<extendedFilteringResources>
						<extendedFilteringResource>
							<inputDataFileSets>
								<param>dataFolder/**/*.yml</param>
							</inputDataFileSets>
							<templatesFileSets>
								<param>templatesFolder/**/*.template</param>
							</templatesFileSets>
							<outputFolder>outputFolder</outputFolder>
							<encoding>UTF-8</encoding>
						</extendedFilteringResource>
					</extendedFilteringResources>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>extended-filtering</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
  [...]
</project>

````

If you intend to configure this mojo for execution on a command line :

```
mvn lab.dev.maven.plugins:devops-utils:1.0.0:extended-filtering
```

## Contributions

We welcome your feature enhancements and bug fixes in pull requests!

